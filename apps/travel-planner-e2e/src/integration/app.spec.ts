import { getGreeting } from '../support/app.po';

describe('travel-planner', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to travel-planner!');
  });
});
