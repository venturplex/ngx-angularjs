import angular from 'angular';
import uiRouter from '@uirouter/angularjs';

import appComponent from './app.component';
import routingService from './routing.service';
import mainModule from './main/main';

import ngRedux from 'ng-redux';
import { RootReducer, accountMiddleware } from './state';

let appModule = angular.module('travel-planner-legacy', [uiRouter, mainModule.name, ngRedux])
  .config(($locationProvider, $stateProvider, $urlRouterProvider, $ngReduxProvider) => {
    'ngInject';
    $locationProvider.html5Mode(true);

    $stateProvider.state('empty', {});

    // show a not found error if route is unknown, but keep the original url
    $urlRouterProvider.otherwise(($injector, $location) => {
      if ($location.path() !== '/') {
        $injector.invoke(($state) => {
          'ngInject';

          $state.transitionTo('empty', {}, false);
        });
      }
    });

    $ngReduxProvider.createStoreWith(RootReducer, [
      // accountMiddleware
    ]);
  })
  .component('travelPlannerLegacyRoot', appComponent)
  .service('RoutingService', routingService);
