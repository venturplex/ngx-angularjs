import * as AccountActions from '../state/account/account.actions';
import { data } from '../state/account/account.data';

class MainController {
  constructor($ngRedux) {
    'ngInject';
    this.headline = 'Hello AngularJS!';
    this.backgroundColor = 'gradient';
    this.isElement = true;
    this.unsubscribe = $ngRedux.connect(this.mapStateToThis, AccountActions)(this);
  }

  $onInit() {
    this.loadAccountSuccess(data);
  }

  $onDestroy(){
    this.unsubscribe();
  }

  mapStateToThis(state) {
    return {
        account: state.account
    };
  }
}

export default MainController;

MainController.$inject = ["$ngRedux"];
