import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import mainComponent from './main.component';

let mainModule = angular
  .module('main', [uiRouter])

  .config(($stateProvider) => {
    'ngInject';

    $stateProvider.state({
      name: 'angularjs',
      url: '/angularjs',
      component: 'angularjs'
    });

  })
  .component('angularjs', mainComponent);

export default mainModule;
