class RoutingService {
  constructor($state) {
    'ngInject';
    this.$state = $state;
  }

  changeRoute(url) {
    this.$state.go(url);
  }
}

export default RoutingService;
