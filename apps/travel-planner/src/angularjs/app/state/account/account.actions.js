export const actionTypes = {
    LoadAccount: '[Account] Load Account',
    LoadAccountSuccess: '[Account] Load Account Success',
    LoadAccountError: '[Account] Load Account Error'
}

export function loadAccount() {
    return {
        type: actionTypes.LoadAccount
    }
}

export function loadAccountSuccess(accountData) {
    return {
        type: actionTypes.LoadAccountSuccess,
        payload: accountData
    }
}

export function loadAccountError(error) {
    return {
        type: actionTypes.LoadAccountError,
        payload: error
    }
}