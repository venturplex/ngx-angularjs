import { actionTypes, loadAccountSuccess } from './account.actions';

/* const middlewares = {
    [actionTypes.LoadAccount]: this.loadAccountMiddleware
}

const loadAccountMiddleware = (action) => {
    return {
        ...action,
        payload: { test: 'this should have a value' }
    }
} */

export const accountMiddleware = store => next => action => {
    if (action.type === actionTypes.LoadAccount) {
        next(loadAccountSuccess({ test: 'this should have a value' }));
    }
}
