import { actionTypes } from './account.actions';

const initialState = {
    data: {},
    error: null
};

export function AccountReducer(state = initialState, action) {
    switch(action.type) {
        case actionTypes.LoadAccountSuccess:
            return { ...state, data: action.payload, error: null };
        case actionTypes.LoadAccountError: {
            return { ...state, error: action.payload };
        }
        default:
            return state;
    }
}