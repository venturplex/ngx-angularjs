import { combineReducers } from 'redux';
import { AccountReducer } from './account';

export const RootReducer = combineReducers({
    account: AccountReducer
});