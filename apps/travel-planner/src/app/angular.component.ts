import { Component } from '@angular/core';

@Component({
  selector: 'travel-planner-angular',
  template: `
    <travel-planner-flights backgroundColor="blue" [isElement]="false"></travel-planner-flights>
    <travel-planner-account backgroundColor="blue" [isElement]="false"></travel-planner-account>
  `,
})
export class AngularComponent {}
