import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AngularComponent } from './angular.component';
import { EmptyComponent } from './empty.component';

const routes: Routes = [
  {path: 'angular', component: AngularComponent},
  {path: '**', component: EmptyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
