import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { tap, filter } from 'rxjs/operators';
import { BridgeService } from './bridge.service';
import { Store } from '@ngrx/store';
import { loadAccountSuccess, reducers } from '@travel-planner/core-state';

@Component({
  selector: 'travel-planner-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Travel Planner Angular';
  unsubscribe;

  constructor(private bridgeService: BridgeService, private router: Router, private store: Store<any>) {}

  ngOnInit(): void {
    window.angular.element(document.body).ready(() => {
      const $ngRedux = this.bridgeService.getInjectable('$ngRedux')
      this.unsubscribe = $ngRedux.connect(this.mapStateToThis.bind(this))(this);
    });

    this.router.events
      .pipe(
        filter(event => event && event instanceof NavigationEnd),
        tap((event: NavigationEnd) => {
          const routingService = this.bridgeService.getInjectable('RoutingService');
          if (routingService) {
            routingService.changeRoute(event.url.replace('/', ''));
          }
        })
      )
      .subscribe();
  }

  mapStateToThis(state) {
    this.store.dispatch(loadAccountSuccess({ payload: state.account.data }));
    return {};
  }
}
