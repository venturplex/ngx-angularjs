import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlightsModule, FlightsWrapperModule } from '@travel-planner/flights';
import { AccountModule, AccountWrapperModule } from '@travel-planner/account';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularComponent } from './angular.component';
import { EmptyComponent } from './empty.component';
import { BridgeService } from './bridge.service';
import { CoreStateModule } from '@travel-planner/core-state';

@NgModule({
  declarations: [AppComponent, AngularComponent, EmptyComponent],
  imports: [
    BrowserModule,
    FlightsModule,
    FlightsWrapperModule,
    AccountModule,
    AccountWrapperModule,
    AppRoutingModule,
    CoreStateModule
  ],
  providers: [BridgeService],
  bootstrap: [AppComponent]
})
export class AppModule {}
