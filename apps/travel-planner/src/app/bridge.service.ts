import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class BridgeService {
  constructor(@Inject(DOCUMENT) private document: Document) { }

  getInjectable(name: string) {
    const injector = window.angular.element(document.body).injector();
    return injector ? injector.get(name) : null;
  }
}
