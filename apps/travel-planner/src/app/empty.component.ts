import { Component } from '@angular/core';

@Component({
  selector: 'travel-planner-empty',
  template: '',
})
export class EmptyComponent {
  constructor() { }
}
