
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { AccountComponent } from './account/account.component';
import { AccountModule } from './account.module';

@NgModule({
  imports: [BrowserModule, AccountModule],
  entryComponents:  [AccountComponent]
})
export class AccountWrapperModule {
  constructor(private injector: Injector) {
    const component = createCustomElement(AccountComponent, {
      injector: this.injector
    });
    customElements.define('travel-planner-account-element', component);
  }

  ngDoBootstrap() { }
}
