import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'travel-planner-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  @Input() backgroundColor = 'grey';
  @Input() isElement = false;

  constructor() { }

  ngOnInit() {
  }

}
