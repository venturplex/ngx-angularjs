# core-state

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test core-state` to execute the unit tests via [Jest](https://jestjs.io).
