import { createAction, props } from '@ngrx/store';

export const loadAccountSuccess = createAction('[Autoloan] Load Account Success', props<{ payload: any }>());

export const loadAccountError = createAction('[Autoloan] Load Account Error', props<{ payload: any }>());
