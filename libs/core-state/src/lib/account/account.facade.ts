import { Injectable } from "@angular/core";
import { Store } from '@ngrx/store';
import { State } from './account.reducer';


@Injectable({
    providedIn: 'root'
})
export class AccountFacadeService {

    constructor(private _store: Store<State>) {}
}