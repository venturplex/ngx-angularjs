import { createReducer, on, Action } from "@ngrx/store";
import * as AccountActions from './account.actions';

export const ACCOUNT_FEATURE_KEY = 'account';

export interface State {
    data: any;
    error: any;
}

export const initialState: State = {
    data: {},
    error: null
}

const _accountReducer = createReducer(
    initialState,
    on(AccountActions.loadAccountSuccess, (state, { payload }) => {
        return { ...state, data: payload };
    }),
    on(AccountActions.loadAccountError, (state, { payload }) => {
        return { ...state, payload };
    })
)

export function reducer(state: State | undefined, action: Action) {
    return _accountReducer(state, action);
}