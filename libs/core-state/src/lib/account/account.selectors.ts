import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAccount from './account.reducer';

export const selectAccountState = createFeatureSelector<fromAccount.State>(fromAccount.ACCOUNT_FEATURE_KEY);

export const selectAccountData = createSelector(
    selectAccountState,
    state => state.data
);

export const selectAccountError = createSelector(
    selectAccountState,
    state => state.error
);
