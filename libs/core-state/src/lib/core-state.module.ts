import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromCoreState from './core-state.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forRoot(fromCoreState.reducers),
        StoreDevtoolsModule.instrument({
          maxAge: 5
        })
    ]
})
export class CoreStateModule {}
