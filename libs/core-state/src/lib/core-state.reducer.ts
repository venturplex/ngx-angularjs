import * as fromAccount from './account/account.reducer';
import { ActionReducerMap } from '@ngrx/store';

export interface State {
    account: fromAccount.State;
}

export const reducers: ActionReducerMap<State, any> = {
    account: fromAccount.reducer
}