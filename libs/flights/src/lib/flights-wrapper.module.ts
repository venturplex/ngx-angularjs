
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { FlightsComponent } from './flights/flights.component';
import { FlightsModule } from './flights.module';

@NgModule({
  imports: [BrowserModule, FlightsModule],
  entryComponents: [FlightsComponent]
})
export class FlightsWrapperModule {
  constructor(private injector: Injector) {
    const component = createCustomElement(FlightsComponent, {
      injector: this.injector
    });
    customElements.define('travel-planner-flights-element', component);
  }

  ngDoBootstrap() { }
}
