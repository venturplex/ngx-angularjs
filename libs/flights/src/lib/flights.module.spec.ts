import { async, TestBed } from '@angular/core/testing';
import { FlightsModule } from './flights.module';

describe('FlightsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FlightsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(FlightsModule).toBeDefined();
  });
});
