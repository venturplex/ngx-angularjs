import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlightsComponent } from './flights/flights.component';

@NgModule({
  imports: [CommonModule],
  declarations: [FlightsComponent],
  exports: [FlightsComponent]
})
export class FlightsModule {}
