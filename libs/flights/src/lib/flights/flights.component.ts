import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'travel-planner-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss']
})
export class FlightsComponent implements OnInit {
  @Input() backgroundColor = 'grey';
  @Input() isElement = false;
  constructor() { }

  ngOnInit() {
  }

}
